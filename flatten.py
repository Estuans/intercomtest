import time

class Timer:
    def __enter__(self):
        self.start = time.clock()
        return self

    def __exit__(self, *args):
        self.end = time.clock()
        self.interval = self.end - self.start

def _qflatten(iterable, append, types):
    for x in iterable:
        if isinstance(x, types):
            _qflatten(x, append, types)
        else: append(x)

def qflatten(iterable):
    flattened = []
    _qflatten(iterable, flattened.append, (list, tuple))
    return flattened

if __name__ == "__main__":
    example_inp = [[1,2,[3]],4]
    with Timer() as t:
        print qflatten(example_inp)
    print "Completed flatten in %.2f sec" % t.interval
