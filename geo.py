"""
Intercom Technical Assessment - Ben Field - v0.1
"""
import json
from math import radians, cos, sin, asin, sqrt
from functools import partial
from pprint import pprint

class PointOfInterest(object):
    """
    PointOfInterest: Nice representation of an point of interest.
    Converts coordinates to floats for convenience.
    """

    name = ""
    lat = 0.0
    lng = 0.0

    def __init__(self, *args, **kwargs):

        self.name = kwargs.get("name")
        self.lat = float(kwargs.get("lat"))
        self.lng = float(kwargs.get("lng"))

class Customer(PointOfInterest):
    """
    Customer: Container object representing users.

    Inherits:
        PointOfInterest
    """

    user_id = None

    def __init__(self, *args, **kwargs):

        self.user_id = kwargs.get("user_id")
        super(Customer, self).__init__(*args, **kwargs)

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return "Customer:{0}:{1}".format(self.user_id, self.name)

class DistanceFinder(object):
    """
    DistanceFinder: Determines the distance between two points stored within
    it's cache.

    Attributes:
        points (dict): A dictionary of points, sourced from a JSON file.
        home (PointOfInterest): A PointOfInterest instance to use as a focal point
    """

    points = {}

    def __init__(self, *args, **kwargs):
        self.home = kwargs.get("home") or PointOfInterest(name="Intercom", lat="53.3381985", lng="-6.2592576")

    def haversine(self, point1, point2, kilometer=True):
        lat1, lng1 = point1
        lat2, lng2 = point2

        # convert lats/longs from degrees to radians
        lat1, lng1, lat2, lng2 = map(radians, (lat1, lng1, lat2, lng2))

        # calculate haversine
        lat = lat2 - lat1
        lng = lng2 - lng1
        d = sin(lat * 0.5) ** 2 + cos(lat1) * cos(lat2) * sin(lng * 0.5) ** 2
        h = 2 * 6371 * asin(sqrt(d))

        if not kilometer:
            return h * 0.621371
        else:
            return h

    def point_to_point(self, point_a, point_b, kilometer=True):
        """Attempt to find the great-circle distance between two points.

        Args:
            point_a (PointOfInterest): The "Source" POI
            point_a (PointOfInterest): The "Destination" POI

        Returns: A float representation of the distance between the two
                 specified points.
        """
        return self.haversine((point_a.lat, point_a.lng),
                              (point_b.lat, point_b.lng),
                              kilometer=kilometer)

    def home_to_point(self, target, kilometer=True):
        """Returns the distance between the DistanceFinder instance's Home
        location, and a target point.

        Args:
            target (PointOfInterest): The target point we are ranging.

        Returns: A float of the distance between the two points.
        """
        return self.point_to_point(self.home, target, kilometer=kilometer)


    def ranged_poi(self, rng, kilometer=True):
        """Returns a list of points of interest sorted alphabetically, within
        the range specified

        Args:
            rng (int):
            km (bool): A boolean parameter that determines whether or not we are
                       measuring distance in KM, or Miles.

        Returns: A list of customer objets sorted by user_id that are within the
                 target radius
        """

        try: # Don't just naively accept what we're given. Users make mistakes.
            rng = int(rng)
        except ValueError:
            raise ValueError("Invalid Range Parameter: Please use a number (float, int).")

        in_range = partial(lambda r, x: self.home_to_point(x, kilometer=kilometer) < r, rng)
        points_in_range = filter(in_range, self.points.values())

        return sorted(points_in_range, key=lambda point: point.user_id)

#This would ideally be called a MarathonRunner :D
class DistanceRunner(DistanceFinder):
    """DistanceRunner: This is the base class for interacting with the DistanceFinder.
    It's purpose is to source the list of customers for processing, and setup the home location.
    """

    def __init__(self, *args, **kwargs):

        try:
            src = kwargs.get("src")
            self.import_poi(src)
        except IOError:
            raise IOError("Unable to load customers from {0}".format(src))

        super(DistanceRunner, self).__init__(*args, **kwargs)

    def import_poi(self, src):
        """ Dummy import method. Must populate self.points with data.
        """
        pass


class FileDistanceRunner(DistanceRunner):
    """FileDistanceRunner: This class sources customers from a JSON file from the local filesystem.

    Inherits:
        DistanceRunner
    """

    def import_poi(self, path):
        with open(path) as srcf:
            points = json.load(srcf)
            for i in points.get("customers"):
                point = Customer(name=i.get("name"),
                                 lat=i.get("latitude"),
                                 lng=i.get("longitude"),
                                 user_id=i.get("user_id"))
                self.points.update({point.name: point})


if __name__ == "__main__":

    intercom = PointOfInterest(name="Intercom", lat="53.3381985", lng="-6.2592576")
    DISTF = FileDistanceRunner(src='res/customers.json', home=intercom)
    print "Customers within 100km of Intercom Office:\n"
    pprint(DISTF.ranged_poi(100))
