import unittest
from geo import FileDistanceRunner, PointOfInterest

class GeoUnitTests(unittest.TestCase):

    df = None

    def setUp(self):
        self.df = FileDistanceRunner(src="res/customers.json")

    def clean_result(self, result):
        return [i.user_id for i in result]

    def test_p_to_p(self):
        """ Test that the distance between two known cities is still calculated correctly.
        """
        geneva = PointOfInterest(name="Geneva", lat="46.2", lng="6.15")
        shanghai = PointOfInterest(name="Shanghai", lat="31.2", lng="121.5")
        res = self.df.point_to_point(geneva, shanghai)

        self.assertAlmostEqual(9238.52173022, res)

    def test_dublin_five_hundred(self):
        """Which users are within 100Km of Dublin? - If this fails, then something has gone seriously wrong...
        """
        golden_value = [4, 5, 6, 8, 11, 12, 13, 15, 17, 23, 24, 26, 29, 30, 31, 39]

        res = self.df.ranged_poi(100)
        self.assertEqual(self.clean_result(res), golden_value)

    def test_london_300_miles(self):
        """What users are within 300 miles of London? - Tests that using miles as a unit works as expected.
        """
        golden_value =  [4, 5, 6, 10, 11, 12, 15, 39]

        london = PointOfInterest(name="London", lat="51.5065205", lng="-0.170416")
        self.df.home = london
        res =  self.df.ranged_poi(300, kilometer=False)

        self.assertEqual(golden_value, self.clean_result(res))

    def test_range_fail(self):
        """Test that setting strings for distances raises a ValueError as intended.
        """
        with self.assertRaises(ValueError):
            self.df.ranged_poi("Hello")


if __name__ == "__main__":
    unittest.main()
